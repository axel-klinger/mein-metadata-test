# Satz des Pythagoras

## Darstellung

![Darstellung des Satz des Pythagoras](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/01-Rechtwinkliges_Dreieck-Pythagoras.svg/370px-01-Rechtwinkliges_Dreieck-Pythagoras.svg.png)  
Grafik "Rechtwinkliges Dreieck, Satz des Pythagoras" von [Petrus3743](https://commons.wikimedia.org/wiki/User:Petrus3743)  unter [CC-BY SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de) aus [Wikipedia](https://commons.wikimedia.org/wiki/File:01-Rechtwinkliges_Dreieck-Pythagoras.svg)

## Die Formel

```math
a^2 + b^2 = c^2
```

## Beispielvideo (Test)

a) Placeholder with Link to Youtube

[![Entwicklungsumgebung mit Groovy/Git testen](https://img.youtube.com/vi/fbZOii_l7M4/maxresdefault.jpg)](https://youtu.be/fbZOii_l7M4)

b) AV-Portal Player embedded

<iframe width="560" height="315" scrolling="no" src="//av.tib.eu/player/40456" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

c) Youtube embedded

<iframe width="420" height="315"
src="https://www.youtube.com/embed/fbZOii_l7M4" allowfullscreen="allowfullscreen">
</iframe>

## H5P Test

<iframe src="https://h5p.org/h5p/embed/617" width="545" height="337" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://h5p.org/sites/all/modules/h5p/library/js/h5p-resizer.js" charset="UTF-8"></script>

...

<iframe src="https://h5p.org/h5p/embed/62954" width="1090" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://h5p.org/sites/all/modules/h5p/library/js/h5p-resizer.js" charset="UTF-8"></script>

## Anwendung

* Beipsiel

## Aufgabe

* Beispielaufgabe mit Lösung
