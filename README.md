# Course Metadata Test

Test HTML metadata for OER repositories and Google Search.

Mit CI werden die folgenden Dokumente erstellt


* [course.epub](https://tibhannover.gitlab.io/oer/course-metadata-test/course.epub)
* [course.pdf](https://tibhannover.gitlab.io/oer/course-metadata-test/course.pdf)
* [course.asc](https://tibhannover.gitlab.io/oer/course-metadata-test/course.asc)
* [course.html](https://tibhannover.gitlab.io/oer/course-metadata-test/index.html)
